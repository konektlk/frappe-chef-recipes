script "Run a script" do
    interpreter "bash"
    code <<-EOH
        curl -fsSL https://get.docker.com -o get-docker.sh
        sh get-docker.sh
        curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
        git clone https://github.com/frappe/frappe_docker.git
        cd frappe_docker
        cp env-production .env
        sed -i -e "s/FRAPPE_VERSION=edge/FRAPPE_VERSION=v13.16.0/g" .env
        sed -i -e "s/ERPNEXT_VERSION=edge/ERPNEXT_VERSION=v13.16.1/g" .env
        sed -i -e "s/email@example.com/anuja@konekt.lk/g" .env
        sed -i -e "s/erp.example.com/erp.konektholdings.com/g" .env
        sed -i -e "s/ADMIN_PASSWORD=admin/ADMIN_PASSWORD=supersecret/g" .env
        sed -i -e "s/MYSQL_ROOT_PASSWORD=admin/MYSQL_ROOT_PASSWORD=longsecretpassword/g" .env
        docker-compose --project-name erpnext up -d
    EOH
  end