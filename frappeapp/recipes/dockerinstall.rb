script "install_docker" do
  interpreter "bash"
  user "root"
  code <<-EOH
  curl -fsSL https://get.docker.com -o get-docker.sh
  sh get-docker.sh
  EOH
end